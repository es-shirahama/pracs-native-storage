import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:bs58/bs58.dart';
import './crypto.dart';

class Provider {
  String host;
  Provider(String host) {
    this.host = host;
  }

  Future <String> signUp(String email, Uint8List signPublicKey, Uint8List boxPublicKey) async {
    try {
      Uri uri = Uri.https(this.host, '/user');
      dynamic headers = { 'content-type': 'application/json' };
      dynamic body = json.encode({
        'email': email,
        'sign_pk': base58.encode(signPublicKey),
        'box_pk': base58.encode(boxPublicKey),
      });
      http.Response response = await http.post(uri, headers: headers, body: body);
      dynamic resBody = json.decode(response.body);
      if (resBody['code'] != 200)
        throw response.body;
      return resBody['data'];
    } catch (e) {
      throw 'provider.signUp => $e';
    }
  }

  Future<Map> getData(String patientId, String attr) async {
    try {
      Uri uri = Uri.https(this.host, '/health/getData', {
        'id': patientId,
        'attr': attr
      });
      http.Response response = await http.get(uri);
      dynamic resBody = json.decode(response.body);
      if (resBody['code'] != 200)
        throw response.body;
      Map data = json.decode(resBody['data']);
      Uint8List patientPk = base58.decode(data['patient_pk']);
      // convert json to instance of EncryptedKey
      List<EncryptedKey> encryptedKeyList = [];
      List<Object> encryptedKeyJSONList = data['encrypted_key_list'];
      for (int i = 0; i < encryptedKeyJSONList.length; i++) {
        encryptedKeyList.add(EncryptedKey.fromJSON(encryptedKeyJSONList[i]));
      }
      Uint8List encryptedData = base58.decode(data['encrypted_data']);
      Uint8List hash = base58.decode(data['hash']);
      Uint8List sig = base58.decode(data['sig']);
      return {
        'patientId': patientId,
        'patientPk': patientPk,
        'encryptedKeyList': encryptedKeyList,
        'attr': attr,
        'encryptedData': encryptedData,
        'hash': hash,
        'sig': sig
      };
    } catch (e) {
      throw 'provider.getData => $e';
    }
  }

  Future<Uint8List> getBoxPk(String id) async {
    try {
      Uri uri = Uri.https(this.host, '/user/getBoxPk', {
        'id': id
      });
      http.Response response = await http.get(uri);
      dynamic resBody = json.decode(response.body);
      if (resBody['code'] != 200)
        throw response.body;
      return base58.decode(resBody['data']);
    } catch (e) {
      throw 'provider.getBoxPk => $e';
    }
  }

  Future<Uint8List> getSignPk(String id) async {
    try {
      Uri uri = Uri.https(this.host, '/user/getBoxPk', {
        'id': id
      });
      http.Response response = await http.get(uri);
      dynamic resBody = json.decode(response.body);
      if (resBody['code'] != 200)
        throw response.body;
      return base58.decode(resBody['data']);
    } catch (e) {
      throw 'provider.getSignPk => $e';
    }
  }

  Future<void> update(String id, Uint8List patientBoxPublicKey, List<EncryptedKey> encryptedKeyList, String attr, Uint8List encryptedData, Uint8List hash, Uint8List sig) async {
    try {
      // convert instance of EncryptedKey to json
      List<Map> encryptedKeyJSONList = [];
      for (int i = 0; i < encryptedKeyList.length; i++) {
        encryptedKeyJSONList.add(encryptedKeyList[i].toJSON());
      }
      Uri uri = Uri.https(this.host, '/health');
      dynamic headers = { 'content-type': 'application/json' };
      dynamic body = json.encode({
        "patient_id": id,
        "patient_pk": base58.encode(patientBoxPublicKey),
        "encrypted_key_list": encryptedKeyJSONList,
        "attr": attr,
        "encrypted_data": base58.encode(encryptedData),
        "hash": base58.encode(hash),
        "sig": base58.encode(sig)
      });
      http.Response response = await http.put(uri, headers: headers, body: body);
      dynamic resBody = json.decode(response.body);
      if (resBody['code'] != 200)
        throw response.body;
    } catch(e) {
      throw 'provider.update => $e';
    }
  }
}