import 'dart:convert';
import 'dart:typed_data';
import 'package:tweetnacl/tweetnacl.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:bs58/bs58.dart';
import './crypto.dart';
import './provider.dart';

final FlutterSecureStorage storage = new FlutterSecureStorage();
final String host = 'elb-stg.pracs.eversystem.co.jp';

class Account {
  // uuid
  String id;
  // https client
  Provider provider;
  // encrypt and decrypt data
  Crypto crypto;

  Account(String id, Uint8List seed, Uint8List boxSecretKey) {
    this.id = id;
    this.crypto = new Crypto(seed, boxSecretKey);
    this.provider = new Provider(host);
  }

  Future<String> readHealthData(String attr) async {
    return await storage.containsKey(key: attr)
        ? await storage.read(key: attr)
        : null;
  }

  Future<void> writeHealthData(String attr, String value) async {
    await storage.write(key: attr, value: value);
  }

  Future<Uint8List> fetchHealthData(String attr) async {
    try {
      // get decoded health data
      Map data = await this.provider.getData(this.id, attr);
      // get box encryption key
      String myBoxPublicKey = base58.encode(this.crypto.boxKeyPair.publicKey);
      // get some data from provider response
      Uint8List encrypted = data['encryptedData'];
      Uint8List patientBoxPublicKey = data['patientPk'];
      List<EncryptedKey> encryptedKeyList = data['encryptedKeyList'];
      // first, decrypted data is empty
      Uint8List decrypted;
      // public key encryption scheme is not updated
      // and
      // patient is me
      if (base58.encode(patientBoxPublicKey) == myBoxPublicKey) {
        EncryptedKey encryptedKey = encryptedKeyList[0];
        Uint8List secretBoxSecretKey = this.crypto.decryptKey(
            encryptedKey.doctorPublicKey, encryptedKey.encryptedKey);
        decrypted = Crypto.decrypt(secretBoxSecretKey, encrypted);
      }
      // public key encryption scheme is updated
      // or
      // doctor
      else {
        for (int i = 0; i < encryptedKeyList.length; i++) {
          EncryptedKey encryptedKey = encryptedKeyList[i];
          if (base58.encode(encryptedKey.doctorPublicKey) ==
              base58.encode(this.crypto.boxKeyPair.publicKey)) {
            Uint8List secretBoxSecretKey = this.crypto.decryptKey(
                encryptedKey.doctorPublicKey,
                encryptedKey.encryptedKey);
            decrypted = Crypto.decrypt(secretBoxSecretKey, encrypted);
            break;
          }
        }
      }
      if (decrypted == null)
        throw 'decrypt failed';
      if (base58.encode(Crypto.hash(decrypted)) != base58.encode(data['hash']))
        throw 'decrypted data was invalid';
      return decrypted;
    } catch (e) {
      throw 'account.fetchHealthData => $e';
    }
  }

  Future<void> updateHealthData(String attr, Uint8List data, List<String> doctorIdList) async {
    try {
      List<EncryptedKey> encryptedKeyList = [];
      for (int i = 0; i < doctorIdList.length; i++) {
        String doctorId = doctorIdList[i];
        Uint8List doctorBoxPublicKey = await this.provider.getBoxPk(doctorId);
        Uint8List encryptedKey = this.crypto.encryptBoxSecretKey(doctorBoxPublicKey);
        encryptedKeyList.add(
            new EncryptedKey(doctorId, doctorBoxPublicKey, encryptedKey));
      }
      Uint8List encryptedData = this.crypto.encrypt(data);
      Uint8List hash = Crypto.hash(data);
      Uint8List sig = this.crypto.sign(hash);
      await this.provider.update(
          this.id,
          this.crypto.boxKeyPair.publicKey,
          encryptedKeyList,
          attr,
          encryptedData,
          hash,
          sig);
    } catch (e) {
      throw 'account.updateHealthData => $e';
    }
  }

  static Future<Account> create(String email) async {
    try {
      if (await storage.containsKey(key: 'account')) {
        throw 'account info already exists';
      }
      // seed value for sign and encrypt key pair
      Uint8List seed = TweetNaclFast.randombytes(32);
      //
      Uint8List boxSecretKey = TweetNaclFast.randombytes(32);
      // ed25519 key pair
      KeyPair signKeyPair = Signature.keyPair_fromSeed(seed);
      // x25519 key pair
      KeyPair boxKeyPair = Box.keyPair_fromSecretKey(seed);
      Provider provider = new Provider(host);
      String id = await provider.signUp(email, signKeyPair.publicKey, boxKeyPair.publicKey);
      dynamic accountInfo = {
        'email': email,
        'id': id,
        'seed': base58.encode(seed),
        'sk': base58.encode(boxSecretKey),
      };
      // save account info
      await storage.write(key: 'account', value: json.encode(accountInfo));
      Account account = new Account(id, seed, boxSecretKey);
      return account;
    } catch (e) {
      throw 'Account.create => $e';
    }
  }

  static Future<Account> load() async {
    try {
      if (await storage.containsKey(key: 'account')) {
        dynamic accountInfo = json.decode(await storage.read(key: 'account'));
        return new Account(
            accountInfo['id'],
            base58.decode(accountInfo['seed']),
            base58.decode(accountInfo['sk'])
        );
      } else {
        throw 'account info not fond in storage';
      }
    } catch (e) {
      throw 'Account.load => $e';
    }
  }
}