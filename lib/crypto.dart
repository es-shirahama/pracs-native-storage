import 'dart:convert';
import 'dart:typed_data';
import 'package:tweetnacl/tweetnacl.dart';
import 'package:bs58/bs58.dart';

class EncryptedKey {
  String doctorId;
  Uint8List doctorPublicKey;
  Uint8List encryptedKey;

  EncryptedKey(String doctorId, Uint8List doctorPublicKey, Uint8List encryptedKey) {
    this.doctorId = doctorId;
    this.doctorPublicKey = doctorPublicKey;
    this.encryptedKey = encryptedKey;
  }

  dynamic toJSON() {
    return {
      'doctor_id': this.doctorId,
      'doctor_pk': base58.encode(this.doctorPublicKey),
      'encrypted_key': base58.encode(this.encryptedKey)
    };
  }
  String toString() {
    return json.encode(this.toJSON());
  }
  static EncryptedKey fromJSON(dynamic json) {
    return new EncryptedKey(
        json['doctor_id'],
        base58.decode(json['doctor_pk']),
        base58.decode(json['encrypted_key'])
    );
  }
  static EncryptedKey fromString(String jsonStr) {
    return fromJSON(json.decode(jsonStr));
  }
}

class Crypto {
  // using for sign data
  KeyPair signKeyPair;
  // using for key sharing
  KeyPair boxKeyPair;
  // using for encrypt health data
  Uint8List secretBoxSecretKey;

  Crypto(Uint8List seed, Uint8List boxSecretKey) {
    this.signKeyPair = Signature.keyPair_fromSeed(seed);
    this.boxKeyPair = Box.keyPair_fromSecretKey(seed);
    this.secretBoxSecretKey = boxSecretKey;
  }

  static randomKeyPair() {
    return Signature.keyPair();
  }

  static keyPairFromSecretKey(Uint8List secretKey) {
    return Signature.keyPair_fromSecretKey(secretKey);
  }

  static Uint8List hash(Uint8List data) {
    return Hash.sha512(data);
  }

  Uint8List encrypt(Uint8List data) {
    return (new SecretBox(this.secretBoxSecretKey)).box(data);
  }

  Uint8List encryptBoxSecretKey(Uint8List theirBoxPublicKey) {
    return (new Box(theirBoxPublicKey, this.boxKeyPair.secretKey)).box(this.secretBoxSecretKey);
  }

  static Uint8List decrypt(Uint8List secretBoxSecretKey, Uint8List encrypted) {
    return (new SecretBox(secretBoxSecretKey)).open(encrypted);
  }

  Uint8List decryptKey(Uint8List theirBoxPublicKey, Uint8List encryptedBoxSecretKey) {
    return (new Box(theirBoxPublicKey, this.boxKeyPair.secretKey)).open(encryptedBoxSecretKey);
  }

  Uint8List sign(Uint8List data) {
    return (new Signature(null, this.signKeyPair.secretKey)).detached(data);
  }

  static bool verify(Uint8List data, Uint8List publicKey, Uint8List sig) {
    return (new Signature(publicKey, null)).detached_verify(hash(data), sig);
  }

}
