import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
// import 'package:curl/curl.dart';
import 'package:bs58/bs58.dart';
import './account.dart';
import './Crypto.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _addressFormKey = GlobalKey<FormState>();
  final _dataFormKey = GlobalKey<FormState>();

  Account account;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text(
              this.account == null
                  ? 'account not loaded'
                  : 'account id: ${this.account.id}',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Row(children: <Widget>[
              Expanded(child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: FlatButton(
                    onPressed: () async {
                      if (this.account == null) {
                        Account account = await Account.load();
                        if (account != null) {
                          setState(() {
                            print('load account info success');
                            this.account = account;
                          });
                        }
                      }
                    },
                    child: Text('load account'),
                  )
              ),),
              Expanded(child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: FlatButton(
                    onPressed: () async {
                      if (this.account != null) {
                        setState(() {
                          print('clear account info success');
                          this.account = null;
                        });
                      }
                    },
                    child: Text('clear account'),
                  )
              ),),
            ]),
            Form(
              key: _addressFormKey,
              child: Column(
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: 'email address'),
                    onSaved: (value) async {
                      print(value);
                      String email = value;
                      Account account = await Account.create(email);
                      if (account != null) {
                        setState(() {
                          this.account = account;
                        });
                      }
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: FlatButton(
                      onPressed: () {
                        _addressFormKey.currentState.save();
                        _addressFormKey.currentState.reset();
                      },
                      child: Text('create new account'),
                    ),
                  ),
                ],
              ),
            ),
            this.account == null
                ? Text('')
                : Column(
                  children: [
                    Form(
                      key: _dataFormKey,
                      child: Column(
                        children: [
                          TextFormField(
                            decoration: InputDecoration(labelText: 'health data'),
                            onSaved: (value) async {
                              await this.account.updateHealthData('info', utf8.encode(value), ['brkrkzymhou']);
                              print('submit health data success');
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: FlatButton(
                              onPressed: () {
                                _dataFormKey.currentState.save();
                                _dataFormKey.currentState.reset();
                              },
                              child: Text('submit health data'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: FlatButton(
                        onPressed: () async {
                          Uint8List data = await this.account.fetchHealthData('info');
                          if (data == null)
                            print(null);
                          else
                            print(utf8.decode(data));
                        },
                        child: Text('get health data'),
                      ),
                    ),
                  ],
                )

          ],
        ),
      ),
    );
  }
}
